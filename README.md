# Prueba Técnica - Programador (Back-end)
La siguiente es una prueba para evaluar a los postulantes a programador **Back-end**.

## INTRODUCCIÓN
Este repositorio contiene una serie de requerimientos de un Caso Práctico, que busca evaluar las capacidades técnicas del candidato con respecto a las principales funciones y responsabilidades que se requieren dentro del área de Investigación y Desarrollo de ladonware.

#### ¿Qué se busca evaluar?
Principalmente los siguientes aspectos:
* Creatividad para resolver los requerimientos,
* Calidad del código entregado (estructura y buenas prácticas),
* Eficiencia de los algoritmos entregados,
* Familiaridad con Frameworks y plataformas de desarrollo.

## IMPORTANTE
1. Recomendamos emplear un máximo de **16 (dieciseis) horas** y enviar todo lo que puedas.
2. Se requiere de una **cuenta de BitBucket** para realizar este ejercicio.
3. **Antes de comenzar a programar:**
    * Realizar un `Fork` de este repositorio (https://bitbucket.org/ladonware/backend-test.git).
    * Clonar el fork a su máquina local  `git clone git@github.com:USERNAME/FORKED-PROJECT.git`
    * Crear un `branch` en su cuenta de BitBucket utilizando su nombre completo.
4. **Al finalizar**, para entregar su proyecto:
    * Realizar un `Commit` de su proyecto, **enviar un `Pull Request` al branch con su NOMBRE**, y notificar a las siguientes direcciones de correo electrónico  [carolina.cetina@ladonware.com](mailto:carolina.cetina@ladonware.com) y [jair.gonzalez@ladonware.com](mailto:jair.gonzalez@ladonware.com).
    

## EJERCICIOS

### Ejercicio #
Descripcion ejercicio


##### Casos de uso
![ Casos de uso](https://cdn-icons-png.flaticon.com/512/1803/1803671.png)

A continuación se describen los casos de uso. No se entra en detalles de la interacción entre el cliente y la aplicación (punto 1 de cada caso de uso), puesto que no va a ser tarea de este ejercicio desarrollar esa parte.

###### **Caso de uso "Caso 1"**


###### **Caso de uso "Caso 2"**


###### **Caso de uso "Caso 3"**
1. El empleado elige la opción "dar de alta vehículo oficial" e introduce su número de placa.
2. La aplicación añade el vehículo a la lista de vehículos oficiales

###### **Caso de uso "Caso 4"**
1. El empleado elige la opción "dar de alta vehículo de residente" e introduce su número de placa.
2. La aplicación añade el vehículo a la lista de vehículos de residentes.

###### **Caso de uso "Caso 5"**
1. El empleado elige la opción "comienza mes".
2. La aplicación elimina las estancias registradas en los coches oficiales y pone a cero el tiempo estacionado por los vehículos de residentes.

###### **Caso de uso "Caso 6"**
1. El empleado elige la opción "genera informe de pagos de residentes" e introduce el nombre del archivo en el que quiere generar el informe.
2. La aplicación genera un archivo que detalla el tiempo estacionado y el dinero a pagar por cada uno de los vehículos de residentes. El formato del archivo será el mostrado a continuación:

```
Núm. placa 	Tiempo estacionado (min.) 	Cantidad a pagar
S1234A 	    20134 				        1006.70
4567ABC	    4896				        244.80
... 		..... 				        .....
```
La aplicación contará con un programa principal basado en un menú que permitirá al empleado interactuar con la aplicación (dicho programa principal no forma parte de este ejercicio).

##### Persistencia de datos
La información de cada una de las estancias de los vehículos será almacenada en una base de datos. Debido a que el manejador de base de datos puede ser modificado en cualquier momento, se utilizará Hibernate como ORM.

##### Puntos que se deben desarrollar
* Diagrama de clases y diagrama de secuencia  de las partes encargadas de la aplicación.
* Aplicación para gestionar las estancias de los vehículos. Deberá incluir:
    * Código de las clases que permitan gestionar los datos asociados.
    * Mapeo de las clases para poder almacenar la información en la base de datos.
    * Clases para gestionar la persistencia de datos, incluida la configuración de conexión a la base de datos.

##### Consideraciones.
*** Utilizar Java 8.
```